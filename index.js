const getCube = 8 ** 3
console.log(`The cube of 8 is ${getCube}`)

const address = [258, "Washington Ave Nw", "California 90011"]
const [streetNumber, street, streetAddres] = address
console.log(`I live at ${streetNumber} ${street}, ${streetAddres}`)


const animalDetails = {
    animalName: "Lolong",
    animalWeight: 1075,
    animalHeight: "20 ft 3 in"
}

const { animalName, animalWeight, animalHeight } = animalDetails
console.log(`${animalName} was a saltwater crocodile. He weight at ${animalWeight} kgs with a measurement of ${animalHeight}.`)


const numbers = [3, 4, 6, 8, 10]
numbers.forEach(number => number)
console.log(numbers)


class Dog {
    constructor(name, age, breed) {
        this.name = name
        this.age = age
        this.breed = breed
    }
}

const myDog = new Dog ("Stig", 2, "American Bully")
console.log(myDog)












